package com.example.testing;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class Hello {


    @GetMapping("/")
    public String helloMessage(Model model) {
        model.addAttribute("message", "Hello, Linux!");
        return "index";
    }
}
