FROM openjdk:18.0-jdk
WORKDIR /app
RUN microdnf install sshpass && microdnf install openssh-clients
COPY ./target/*.jar ./ROOT.jar
ENTRYPOINT  ["java","-jar","ROOT.jar"]
EXPOSE 8080
